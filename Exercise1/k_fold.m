function sample = k_fold(k, K, data)
% apply k-fold corss Validation 
% divee the data in k equal sized subsamels. Training is perfomed k
% times(folds) where in each fold (k-1) subsamples are used for training
% and the remining subsamel is used for testing 

% k: number of equl sized subsamels 
% K: sampel from the k subsmales used for testing 
% data: Data to perform k_fold corss validaiton 
 
    if K > k 
        disp("Error: specified test samole out of Range")
        return; 
    end 
    
    if mod(size(data,2), k) ~= 0
        sprintf("Data cant exactly be splitted into %d subsamples with equal size \n -> Data that dont fit into the last subsample will be excluted", k)
    end 
    
    bin_size = size(data,2)/k;
    k_input = zeros(size(data,1), floor(bin_size));
    for i = 1:k
        k_input(:, :, i) = data(:, 1+(i-1)*floor(bin_size):i*floor(bin_size));
    end 
    test_sample = k_input(:, :, K);
    k_input(:, :, K) = [];
    train_sample = reshape(k_input, [size(data,1), size(k_input,2)*size(k_input,3)]);
    sample = {train_sample, test_sample};
    
end 