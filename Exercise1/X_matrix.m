function X = X_matrix(p, input_data)
% Create the linear regression X-Matrix 

X = input_data;
X = [ones(1,size(X,2)); X];
X = [X; X(2,:).*X(3,:)];
if p >= 2
    for n = 2:p
        X = [X; X(2,:).^n]; % v^p
        X = [X; X(3,:).^n]; % w^p
        X = [X; X(4,:).^n]; % (v*w)^p
    end 
end 

end 

