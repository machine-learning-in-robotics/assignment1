function par = Exercise1(k)
% k: number of folds for k fold Cross Validation 
% par: Cell Array of size 1x3. Each cell par{i} contains the learned
% parameters values a_i1, a_i2, ..., a_im as column vectors 

% load given Dataset 
load('Data.mat', 'Input', 'Output')

% Init Variables 
max_p = 6;
xy_average_error = Inf(1,max_p);
theta_average_error = Inf(1,max_p);

for p = 1:max_p
    
    xy_error_sum = 0; 
    theta_error_sum = 0; 
    
    for i = 1:k
     
        % get train and test samples with k-fold cross validation 
        input_samples = k_fold(k, i, Input);
        input_train_sample = input_samples{1};
        input_test_sample = input_samples{2};
        
        output_samples = k_fold(k, i, Output);
        output_train_sample = output_samples{1};
        output_test_sample = output_samples{2};
        
        % Create the linear regression X-Matrix
        X_train = X_matrix(p, input_train_sample);
        X_test =  X_matrix(p, input_test_sample);
        
        % Postion
        % Optimization
        xy_weights = (X_train*X_train')\(X_train*output_train_sample(1:2,:)'); 
        % Evaluate regresssion model 
        xy_pred = X_test'*xy_weights; 
        % Calulate Error betwen real data and prediction  
        xy_error = sum(sqrt(((output_test_sample(1,:)'-xy_pred(:,1)).^2) + (output_test_sample(2,:)'-xy_pred(:,2)).^2))/size(output_test_sample(1:2,:), 2); 
        xy_error_sum = xy_error_sum + xy_error;
        
        % Orientation
        % Optimization 
        theta_weights = (X_train*X_train')\(X_train * output_train_sample(3,:)');
        % Evaluate regresssion model 
        theta_pred = X_test'*theta_weights;
        % Calulate Error betwen real data and prediction  
        theta_error = sum(sqrt((output_test_sample(3,:)' - theta_pred).^2))/size(output_test_sample(3,:), 2);    
        theta_error_sum = theta_error_sum + theta_error;
        
    end 
    % Calcuate overall estimation error for one p-value  
    xy_average_error(p) = xy_error_sum/k; 
    theta_average_error(p) = theta_error_sum/k;
        
end

[~,p1] = min(xy_average_error);
[~,p2] = min(theta_average_error);

% re-estimation of model parametes for the selected model complexity with the entire dataset
% Postion
X_train = X_matrix(p1, Input); 
% Optimization 
xy_weights = (X_train*X_train')\(X_train*Output(1:2,:)'); 

% Orientation
X_train = X_matrix(p2, Input);
% Optimization 
theta_weights = (X_train*X_train')\(X_train * Output(3,:)');

% output 
disp(['p1: ', p1])
disp(['p2: ', p2])
disp('xy_weights: ')
disp(xy_weights)
disp('theta_weights: ')
disp(theta_weights)

par = {xy_weights(:,1), xy_weights(:,2), theta_weights};
save("params","par");

% simulate Robot
Simulate_robot(0.5,-0.03);

end 