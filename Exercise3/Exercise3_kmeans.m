  function [] = Exercise3_kmeans(data1, data2, data3, init_data1, init_data2, init_data3, num_cluster)

data1 = reshape(data1,[600,3]);
data2 = reshape(data2,[600,3]);
data3 = reshape(data3,[600,3]);

[labels1, code_vec1] = k_means(data1, init_data1, num_cluster, true);
[labels2, code_vec2] = k_means(data2, init_data2, num_cluster, true);
[labels3, code_vec3] = k_means(data3, init_data3, num_cluster, true);

plot_points(data1, labels1{1}, 'K-means clusterd Gesture-l')
plot_points(data2, labels2{1}, 'K-means clusterd Gesture-o')
plot_points(data3, labels3{1}, 'K-means clusterd Gesture-x')

end 

function [labels, code_vec] = k_means(data, init_code_vec, k, plot)

% Initialise some Variables 
code_vec = init_code_vec; 
n = size(data,1);
dist = Inf(n, k);
decrement = Inf; 
J_old = Inf; 

if plot 
    color_map = ['b', 'k', 'r', 'g', 'm', 'y', 'c'];
    figure; 
end 

 while decrement > 10e-6
     
    % Finde the Closest Points to the CV (e-step) by calculating the eucledian distance  
    for i = 1:k 
        dist(:, i) = sqrt((data(:,1)-code_vec(i,1)).^2 + (data(:,2)-code_vec(i,2)).^2 + (data(:,3)-code_vec(i,3)).^2);
    end
    [dist_min, labels] = min(dist,[], 2); 
     
    % Update the code vectors to the new cluster centers
    for i = 1:k 
        cluster = data(labels == i, :);
        code_vec(i,:) = mean(cluster);
    end 
    
    % Calculate total distortion by summing up all the minimum distances to
    % the new cluster centers and calcualte the decrement, choosen
    % distoriton function: Euclidian distance 
    J = sum(dist_min);
    decrement = J_old - J;
    J_old = J; 
  
    if plot 
        % print clusters and code vectors 
        cla;
        for i = 1:k
            cluster = data(labels == i, :);
            h1 = scatter3(cluster(:,1), cluster(:,2),cluster (:,3), color_map(i));
            h1.MarkerFaceColor = color_map(i);
            hold on;
            h2 = scatter3(code_vec(i,1), code_vec(i,2), code_vec(i,3), 200, [0.5, 0.5, 0.5], 'd');
            h2.MarkerFaceColor = [0.5; 0.5; 0.5];
            text(code_vec(i,1)+6, code_vec(i,2)+6, code_vec(i,3)+6, sprintf('CV %d', i));        
            hold on;
        end  
        view(2);
        title('K-means clusterd Data');
        xlabel('X');
        ylabel('Y'); 
        zlabel('Z');
        view(2);
        drawnow; 
    end 
    
 end 
 
code_vec = {code_vec};
labels = {labels};
end 