function plot_points(data, labels, graph_title)
% print clusters and code vectors 

figure; 
color_map = ['b', 'k', 'r', 'g', 'm', 'y', 'c'];

for i = 1:max(max(labels))
    cluster = data(labels == i, :);
    h1 = scatter3(cluster(:,1), cluster(:,2),cluster (:,3), color_map(i));
    h1.MarkerFaceColor = color_map(i);
    hold on;
end

view(2);
title(graph_title);
xlabel('X');
ylabel('Y'); 
zlabel('Z');