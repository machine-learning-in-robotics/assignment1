function [] = Exercise3_nubs(data1, data2, data3, num_cluster)

data1 = reshape(data1, [600, 3]);
data2 = reshape(data2, [600, 3]);
data3 = reshape(data3, [600, 3]); 

split_vec = [0.08, 0.05, 0.02]; 

[labels1, code_vec1] = nubs(data1, split_vec, num_cluster, true);
[labels2, code_vec2] = nubs(data2, split_vec, num_cluster, true);
[labels3, code_vec3] = nubs(data3, split_vec, num_cluster, true);

plot_points(data1, labels1{1}, 'n-ubs clusterd Gesture-l')
plot_points(data2, labels2{1}, 'n-ubs clusterd Gesture-o')
plot_points(data3, labels3{1}, 'n-ubs clusterd Gesture-x')


end 

function [labels, code_vec] = nubs(data, split_vec, num_cluster, plot)

% Initialize some Variables
k = 1; 
labels = ones(size(data,1), 1);
    
% init code_vector is the mean of the entire data 
code_vec = mean(data);

if plot 
    color_map = ['b', 'k', 'r', 'g', 'm', 'y', 'c'];
    figure; 
end 

while k < num_cluster 

    distortion = zeros(k, 1);
    for i = 1:k
        cluster = data(labels == i, :);
        % Get disortion value of the cluster, by calculating the mean of all distances to the CV of the class   
        dist = sqrt((cluster(:,1)-code_vec(i,1)).^2 + (cluster(:,2)-code_vec(i,2)).^2 + (cluster(:,3)-code_vec(i,3)).^2);
        distortion(i) = sum(dist);
    end 
 
    % Choose the cluster with the largest disortion
    [max_distortion, max_label] = max(distortion);
    
    % Add split vect to code_vector witch belongs to the choosen cluster
    a = code_vec(max_label, :) + split_vec; 
    b = code_vec(max_label, :) - split_vec; 
    
    for i = 1:size(data,1)
        % check if points belongs to the relvant points form cluster with
        % maximum distortion
        if labels(i) == max_label
            dist_a = sqrt((data(i,1)-a(1)).^2 + (data(i,2)-a(2)).^2 + (data(i,3)-a(3)).^2);
            dist_b = sqrt((data(i,1)-b(1)).^2 + (data(i,2)-b(2)).^2 + (data(i,3)-b(3)).^2);
            % update data labels
            if dist_a < dist_b 
                labels(i) = max_label; 
            else 
                labels(i) = k+1; 
            end 
        end 
    end 
    
    % Update the Code Vecotrs s
    code_vec(max_label, :) = mean(data(labels == max_label, :));
    code_vec(k+1, :) = mean(data(labels == k+1, :)); 
    
    k = k+1; 
    
    if plot 
        % print clusters and code vectors 
        cla;
        for i = 1:size(code_vec, 1)
            cluster = data(labels == i, :);
            h1 = scatter3(cluster(:,1),cluster(:,2),cluster(:,3), color_map(i));
            h1.MarkerFaceColor = color_map(i);
            hold on; 
            h2 = scatter3(code_vec(i,1), code_vec(i,2), code_vec(i,3), 200, [0.5, 0.5, 0.5], 'd');
            h2.MarkerFaceColor = [0.5; 0.5; 0.5];
            text(code_vec(i,1)+6, code_vec(i,2)+6, code_vec(i,3)+6, sprintf('CV %d', i));        
            hold on; 
        end 
        hold off; 

        title('Non-Uniform Binary Splited Data');
        xlabel('X');
        ylabel('Y'); 
        zlabel('Z');
        view(2);
        drawnow;
    end 
end

code_vec = {code_vec};
labels = {labels};
end 


