function [confusion_matrix, d_optimal, smallest_error] = Exercise2(d_max)

images = loadMNISTImages('train-images.idx3-ubyte');
labels = loadMNISTLabels('train-labels.idx1-ubyte');

test_images = loadMNISTImages('t10k-images.idx3-ubyte');
test_labels = loadMNISTLabels('t10k-labels.idx1-ubyte');

% Calculate Feature Mean Vector from training Data
mean_vec = mean(images, 2);

% Make the traing data zero mean
for i = 1:size(images,1)
    images(i, :) = images(i,:) - mean_vec(i);
end 

% Make the test data zero mean, based on the traing data feature mean
for i = 1:size(test_images,1)
    test_images(i, :) = test_images(i, :) - mean_vec(i);
end 

% Calculate Eigenvalues and Eigenvectors of the Covariance Matrix 
S = cov(images');
[V, D] = eig(S);

% Create Projetkon Matrix W with d_max Eigenvecotors sorted by the 
% highest Eigenvalues
[largest_D, largest_D_ind] = maxk(diag(D), d_max);
[sorted_largest_D, sort_ind] = sort(largest_D, 'descend');
W = V(:, largest_D_ind(sort_ind));

% Apply the ML-classifier on the test data by changing the value of 
% d from 1 to 60 and calculate the classification error
error_vec = Inf(1, d_max);
smallest_error = Inf; 
d_optimal = 0; 
for d = 1:d_max
    % display act d value 
    disp(['d-value: ', num2str(d)])
    % Project the data 
    pca_data = W(:, 1:d)' * images; 
    
    % calcualte the mean and covariances of each digit 
    class_mean = cell(1, 10);
    class_cov = cell(1, 10);
    for i = 0:9
    class_mean{i+1} = mean(pca_data(:, labels==i), 2);
    class_cov{i+1} = cov(pca_data(:, labels==i)');
    end 
    
    % Project the training data on already learned basis
    pca_test_data = W(:, 1:d)' * test_images;
    
    % Calculate the likelyhood value of the projected test data for each 
    % class 
    prop_matrix = Inf(size(test_images, 2), 10);
    for i = 1:size(test_images, 2)
        for j = 1:10
            prop_matrix(i, j) = mvnpdf(pca_test_data(:, i), class_mean{j}, class_cov{j});
        end 
    end 
    
    % get predicted class, by choosing the calls regaring to the input with
    % the hightes likelihood value 
    [M,I] = max(prop_matrix, [], 2);
    pred_labels = I-1;
    % Calcualte the Classification Error for the actuall d (in %)
    error_vec(d) = mean(~(test_labels == pred_labels))*100;
    if error_vec(d) < smallest_error
        d_optimal = d;
        smallest_error = error_vec(d); 
        confusion_matrix = confusionmat(test_labels, pred_labels);
    end 
    
end 

figure; 
ln = plot(error_vec);
ln.LineWidth = 2;
ln.Color = 'r';
ln.Marker = 'o';
ln.MarkerEdgeColor = 'b';

title('Plot of Classificaiton Error when varying d from 1 to d_{max}')
xlabel('d-value') 
ylabel('classification error in %') 

disp('*****************************');
disp('Results Exercise 2');
disp(['Optimal d-value: ', num2str(d_optimal)]);
disp(['Minimum error: ', num2str(smallest_error),' %']);
disp('Confusion Matrix for optimal d-value: ');
helperDisplayConfusionMatrix(confusion_matrix)

d_optimal = {d_optimal};
smallest_error = {smallest_error};
confusion_matrix = {confusion_matrix}; 
    
end 


